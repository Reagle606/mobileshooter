﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EasyUI
{
    [System.Serializable]
    public class Bar : MonoBehaviour
    {
        public Skin m_barSkin;

        [SerializeField]
        public bool customBarSize;

        public Color m_fillColour = Color.red;
        public Color m_decayColour = Color.blue;

        [Tooltip("Will the percentage display on the bar")]
        public bool usePercentageText;
        [Tooltip("Will the amount display on the bar")]
        public bool useAmountText;

        public bool useTimerText;
        public bool useCustomStringText;

        [Tooltip("Does the bar display a slight decay on value change")]
        public bool m_useDecay;

        [Header("Speed Values")]
        [Tooltip("The time, in Seconds, that it will take the fill to reach the current value")]
        public float m_updateSpeed = 0.2f;
        [Tooltip("The time, in Seconds, that it will take the decay to reach the current value")]
        public float m_decaySpeed = 0.2f;
        [Tooltip("The delay, in Seconds, before the decay will update after a value change")]
        public float m_decayDelay;

        public Text percentageText, amountText, customStringText;

        protected Coroutine decayCorotuine;
        protected Coroutine updateCoroutine;
        public bool inverted;

        public virtual void Start()
        {
            UpdateSkin();
        }
        public virtual void UpdateSkin()
        {

        }
        public void UpdateText(float a_percentage)
        {
            if (percentageText != null)
            {
                 percentageText.text = (a_percentage * 100).ToString("#.0") + "%";
            }
        }
        public void UpdateText(float a_current, float a_max)
        {
            if (amountText != null)
            {
                amountText.text = a_current + " / " + a_max;
            }
        }
        public void UpdateText(string a_text)
        {
            if(customStringText != null)
            {
                customStringText.text = a_text;
            }
        }
         
        public virtual void UpdateBar(float a_percentage)
        {
            if (this.gameObject.activeSelf)
            {
                if (updateCoroutine != null)
                {
                    StopCoroutine(updateCoroutine);
                }
                updateCoroutine = StartCoroutine(UpdateValue(a_percentage));
                if (m_useDecay)
                {
                    if (decayCorotuine != null)
                    {
                        StopCoroutine(decayCorotuine);
                    }
                    decayCorotuine = StartCoroutine(Decay(a_percentage));
                }
            }
        }
        public virtual void UpdateBar(float a_current, float a_max, float a_min = 0)
        {
            if (this.gameObject.activeSelf)
            {
                float percentage = a_current / a_max;
                if (updateCoroutine != null)
                {
                    StopCoroutine(updateCoroutine);
                }
                updateCoroutine = StartCoroutine(UpdateValue(percentage));
                if (m_useDecay)
                {
                    if (decayCorotuine != null)
                    {
                        StopCoroutine(decayCorotuine);
                    }
                    decayCorotuine = StartCoroutine(Decay(percentage));
                }
            }
        }

        public virtual IEnumerator UpdateValue(float a_endValue)
        {
            yield return null;
        }
        public virtual IEnumerator Decay(float a_endValue)
        {
            yield return null;
        }
    }
}
