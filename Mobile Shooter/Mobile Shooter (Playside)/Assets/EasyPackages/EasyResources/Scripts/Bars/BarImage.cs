﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EasyUI
{
    [ExecuteInEditMode]
    public class BarImage : MonoBehaviour
    {
        public Image m_backing, m_fill, m_decay, m_mask;
        public Vector2 imagePercentageRange;
        public bool autoAssingImages = true;

        public void Awake()
        {
            if (autoAssingImages)
            {
                AssignImages();
            }
        }

        public void AssignImages()
        {
            m_backing = transform.Find("Backing")?.GetComponent<Image>();
            m_mask = m_backing.transform.Find("Mask")?.GetComponent<Image>();
            m_fill = m_mask.transform.Find("Fill")?.GetComponent<Image>();
            m_decay = m_mask.transform.Find("Decay")?.GetComponent<Image>();

        }
    }
}
