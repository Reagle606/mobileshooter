﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EasyUI
{
    [ExecuteInEditMode]
    public class FillBar : Bar
    {
        public BarImage m_image;

        public override void UpdateSkin()
        {
            if (m_barSkin != null)
            {
                m_image.m_backing.sprite = m_barSkin.m_backing;
                m_image.m_mask.sprite = m_barSkin.m_mask;
                m_image.m_fill.sprite = m_barSkin.m_fill;
                m_image.m_decay.sprite = m_barSkin.m_decay;
                m_image.m_fill.color = m_fillColour;
                m_image.m_decay.color = m_decayColour;

                if (!customBarSize)
                {
                    m_image.m_backing.rectTransform.sizeDelta = m_barSkin.m_backingSize;
                    m_image.m_mask.rectTransform.sizeDelta = m_barSkin.m_maskSize;
                    m_image.m_decay.rectTransform.sizeDelta = m_barSkin.m_decaySize;
                    m_image.m_fill.rectTransform.sizeDelta = m_barSkin.m_fillSize;
                }

                percentageText = m_image.m_mask.transform.Find("PercentageText")?.GetComponent<Text>();
                amountText = m_image.m_mask.transform.Find("AmountText")?.GetComponent<Text>();
                customStringText = m_image.m_mask.transform.Find("CustomText")?.GetComponent<Text>();
               
            }
        }

        public override void UpdateBar(float a_percentage)
        {
            base.UpdateBar(a_percentage);
        }
        public override void UpdateBar(float a_current, float a_max, float a_min = 0)
        {
            base.UpdateBar(a_current, a_max, a_min);
        }

        public override IEnumerator UpdateValue(float a_endValue)
        {
            float timer = 0;
            float startValue = m_image.m_fill.fillAmount;
            if(inverted)
            {
                a_endValue = Mathf.Abs(a_endValue - 1);
            }
            if (a_endValue > startValue)
            {
                yield return new WaitForSeconds(m_decayDelay);
            }
            while (timer < m_updateSpeed)
            {
                timer += Time.deltaTime;
                m_image.m_fill.fillAmount = Mathf.Lerp(startValue, a_endValue, timer / m_updateSpeed);
                yield return null;
            }
        }
        public override IEnumerator Decay(float a_endValue)
        {
            float timer = 0;
            float startValue = m_image.m_decay.fillAmount;
            if (inverted)
            {
                a_endValue = Mathf.Abs(a_endValue - 1);
            }
            if (a_endValue < startValue)
            {
                yield return new WaitForSeconds(m_decayDelay);
            }
            while (timer < m_decaySpeed)
            {
                timer += Time.deltaTime;
                m_image.m_decay.fillAmount = Mathf.Lerp(startValue, a_endValue, timer / m_decaySpeed);
                yield return null;
            }
        }

    }
}
