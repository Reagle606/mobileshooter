﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EasyUI
{
    public class ImageBar : Bar
    {
        public List<BarImage> m_images;

        public float ImagePercentage { get { return (100 / (float)m_images.Count) / 100; } }

        public override void UpdateSkin()
        {
            foreach (BarImage i in m_images)
            {
                if (m_barSkin != null)
                {
                    i.m_backing.sprite = m_barSkin.m_backing;
                    i.m_mask.sprite = m_barSkin.m_mask;
                    i.m_fill.sprite = m_barSkin.m_fill;
                    i.m_decay.sprite = m_barSkin.m_decay;
                    i.m_fill.color = m_fillColour;
                    i.m_decay.color = m_decayColour;
                }
            }
        }
        public override void UpdateBar(float a_percentage)
        {
            base.UpdateBar(a_percentage);
        }
        public override void UpdateBar(float a_current, float a_max, float a_min = 0)
        {
            base.UpdateBar(a_current, a_max, a_min);
        }

        public override void Start()
        {
            m_images.Clear();
            BarImage[] images = GetComponentsInChildren<BarImage>();
            foreach (BarImage i in images)
            {
                m_images.Add(i);
            }
            base.Start();
        }
        public override IEnumerator UpdateValue(float a_endValue)
        {
            if (inverted)
            {
                a_endValue = Mathf.Abs(a_endValue - 1);
            }
            for (int i = 0; i < m_images.Count; i++)
            {
                m_images[i].imagePercentageRange = new Vector2(ImagePercentage * i, ImagePercentage * (i + 1));

                if (a_endValue > ImagePercentage * i + 0.01f)
                {
                    m_images[i].m_fill.fillAmount = 1;
                }
                else
                {
                    m_images[i].m_fill.fillAmount = 0;
                }

            }

            yield return null;
        }
        public override IEnumerator Decay(float a_endValue)
        {
            yield return null;
        }

    }
}
