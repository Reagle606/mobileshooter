﻿using System.Collections;
using UnityEngine;

namespace EasyUI
{
    [CreateAssetMenu(menuName = "Skins/New Skin")]
    public class Skin : ScriptableObject
    {
        public Sprite m_backing, m_fill, m_decay, m_mask;
        public Vector2 m_backingSize, m_maskSize, m_decaySize, m_fillSize;
    }
}