﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EasyResources
{
    public class EasyResourceManager : MonoBehaviour
    {
        public static EasyResourceManager Instance;
        public GameObject popup;

        public void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        public GameObject canvas;

        public void CreatePopup(float a_time, Vector2 a_position, string a_text)
        {
            if(canvas == null)
            {
                Debug.LogWarning("There is no canvas assigned, please assign a canvas in order to create popups");
                return;
            }
            InfoPopup pop = Instantiate(popup, canvas.transform).GetComponent<InfoPopup>();
            pop.SetTime(2);
            pop.SetText(a_text);
            pop.transform.position = a_position;
            pop.OnSpawn();
        }
    }
  
}
