﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using EasyUI;

[CustomEditor(typeof(FillBar))]
public class FillBarCustomInspector : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        FillBar bar = (FillBar)target;

        if (GUILayout.Button("UpdateSkin"))
        {
            bar.UpdateSkin();
        }
        if (GUILayout.Button("AssignImages"))
        {
            bar.m_image.AssignImages();
        }
    }
}


[CustomEditor(typeof(ImageBar))]
public class ImageBarCustomInspector : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        ImageBar bar = (ImageBar)target;

        if (GUILayout.Button("UpdateSkin"))
        {
            bar.UpdateSkin();
        }
        if (GUILayout.Button("AssignImages"))
        {
            foreach (BarImage i in bar.m_images)
            {
                i.AssignImages();
            }

        }
    }
}

