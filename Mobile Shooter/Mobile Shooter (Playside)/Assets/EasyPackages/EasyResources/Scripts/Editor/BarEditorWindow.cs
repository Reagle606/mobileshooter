﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using EasyUI;
using UnityEngine.SceneManagement;

public class BarSelectionWindow : EditorWindow
{
    Vector2 scrollpos;

    [MenuItem("Window/Bar Window")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow.GetWindow(typeof(BarSelectionWindow));
    }

    void OnGUI()
    {
        Object[] bars = FindObjectsOfType(typeof(Bar));

        scrollpos = GUILayout.BeginScrollView(scrollpos, GUILayout.Height(300));
        foreach (Bar b in bars)
        {
            GUILayout.BeginHorizontal(GUILayout.Width(400));
            if (GUILayout.Button(b.name, GUILayout.Width(200)))
            {
                Selection.activeGameObject = b.gameObject;
                BarEditorWindow.ShowWindow(b);
            }
         
            GUILayout.EndHorizontal();
        }
        GUILayout.EndScrollView();
        if(GUILayout.Button("Create New Bar"))
        {
            CreateNewBar();
        }
    }

    public void CreateNewBar()
    {
        EasyResources.EasyResourceManager manager = GameObject.Find("EasyResourceManager").GetComponent<EasyResources.EasyResourceManager>();
        Bar bar = AssetDatabase.LoadAssetAtPath("Assets/EasyPackages/EasyResources/Prefabs/BarTemplate.prefab", typeof(Bar)) as Bar;
        PrefabUtility.InstantiatePrefab(bar, manager.canvas.transform);
    }
}

public class BarEditorWindow : EditorWindow
{
    public Bar selectedBar;

    public static void ShowWindow(Bar a_selectedBar)
    {
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow.GetWindow(typeof(BarEditorWindow));
    }

    void OnGUI()
    {
        if (Selection.activeGameObject != null)
        {
            if (Selection.activeGameObject.GetComponent<Bar>())
            {
                selectedBar = Selection.activeGameObject.GetComponent<Bar>();
            }
        }
        if (selectedBar != null)
        {
            GUILayout.Label(selectedBar.name);
            GUILayout.BeginHorizontal();
            GUILayout.Label("Primary Colour");
            selectedBar.m_fillColour = EditorGUILayout.ColorField(selectedBar.m_fillColour);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Decay Colour");
            selectedBar.m_decayColour = EditorGUILayout.ColorField(selectedBar.m_decayColour);
            GUILayout.EndHorizontal();


            GUILayout.BeginHorizontal();
            selectedBar.customBarSize = GUILayout.Toggle(selectedBar.customBarSize, "Use Custom Size");
          
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            if (selectedBar.customBarSize)
            {
                Vector2 barSize = EditorGUILayout.Vector2Field("Size", ((FillBar)selectedBar).m_image.m_backing.rectTransform.sizeDelta);
                ((FillBar)selectedBar).m_image.m_backing.rectTransform.sizeDelta = barSize;
                ((FillBar)selectedBar).m_image.m_fill.rectTransform.sizeDelta = barSize;
                ((FillBar)selectedBar).m_image.m_decay.rectTransform.sizeDelta = barSize;
                ((FillBar)selectedBar).m_image.m_mask.rectTransform.sizeDelta = barSize;
            }
            GUILayout.EndHorizontal();
            EditorUtility.SetDirty(selectedBar);
            selectedBar.UpdateSkin();
         

        }
    }
}
