﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoPopup : MonoBehaviour
{
    public Text m_text;
    public float popupTime;

   
    public void OnSpawn()
    {
        Destroy(this.gameObject, popupTime);
    }

    public void SetPosition(Vector2 a_pos)
    {
        transform.position = a_pos;
    }
    public void SetText(string a_text)
    {
        m_text.text = a_text;
    }
    public void SetTime(float a_time)
    {
        popupTime = a_time;
    }

}
