﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyUI;

namespace EasyResources
{
    [System.Serializable]
    public class Resource  
    {
        public List<Bar> bars = new List<Bar>();

        public float m_max, m_min, m_current;

        public float percentage { get { return m_current / m_max; } }

        public bool isEmpty { get { return m_current <= m_min; } }

        public bool setToMaxOnStart;

        public bool clampRange;

        public string customString;

        public Trigger emptyTrigger = new Trigger();

        private bool initialised;
        
        public Resource()
        {
            Initialise();
        }
  
        public virtual void Initialise()
        {
            emptyTrigger = new Trigger();
            if (setToMaxOnStart)
            {
                m_current = m_max;
            }
            UpdateBars();
            initialised = true;
        }

        /// <summary>
        /// Adds the passed in value to the current value
        /// </summary>
        /// <param name="a_change"></param>
        public void AdjustValue(float a_change)
        {
            m_current += a_change;
            if (clampRange)
            {
                m_current = Mathf.Clamp(m_current, m_min, m_max);
            }
            UpdateBars();
        }

        /// <summary>
        /// Sets the value of the bar to a specified value
        /// </summary>
        /// <param name="a_value"></param>
        public void SetValue(float a_value)
        {
            m_current = a_value;
            UpdateBars();
        }
        /// <summary>
        /// Updates all Linked Bars to match the percentage value of the bar
        /// </summary>
        public void UpdateBars()
        {
            if (bars != null)
            {
                foreach (Bar b in bars)
                {
                    if (b != null)
                    {
                        b.UpdateBar(percentage);
                        if (b.useAmountText)
                            b.UpdateText(m_current, m_max);
                        if (b.usePercentageText)
                            b.UpdateText(percentage);
                        if (b.useCustomStringText)
                            b.UpdateText(customString);
                    }
                }
            }
            
            if (isEmpty && !emptyTrigger.triggered && initialised)
            {
                emptyTrigger.SetTrigger();
            }
        }
    }
}
