﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace EasyResources
{
    [System.Serializable]
    public class Timer : Resource
    {
        public UnityEvent m_onTimerStart = new UnityEvent();
        public UnityEvent m_onTimerTick = new UnityEvent();
        public UnityEvent m_onTimerEnd = new UnityEvent();

        private Coroutine timerCoroutine;
        private Coroutine updateBarCoroutine;

        public Trigger endTrigger = new Trigger();
        public Trigger startTrigger = new Trigger();
        public Trigger tickTrigger = new Trigger();

        public bool isLoop;
        public bool reverse;
       
        public float tickInterval = 0;
        public bool useTimerText;
     
        public bool m_paused;
        public bool m_running;

        public override void Initialise()
        {
            base.Initialise();
            endTrigger = new Trigger();
            startTrigger = new Trigger();
            tickTrigger = new Trigger();       
        }

        public Timer()
        {
            Initialise();
        }
        public Timer(float a_max, float a_min = 0)
        {
            m_max = a_max;
            m_min = a_min;
        }

        public void StartTimer()
        {
            if (!m_running)
            {
                timerCoroutine = EasyResourceManager.Instance.StartCoroutine(TickCoroutine());
                OnTimerStart();
            }
            if (m_paused)
            {
                m_paused = false;
            }
        }
        public void RestartTimer()
        {
            if (m_running)
            {
                ResetTimer();
                m_paused = false;
            }
            else
            {
                StartTimer();
            }

        }

        public void StopTimer()
        {
            EasyResourceManager.Instance.StopCoroutine(timerCoroutine);
            EasyResourceManager.Instance.StopCoroutine(updateBarCoroutine);
            m_paused = false;
            m_running = false;
            ResetTimer();

        }

        public void ResetTimer()
        {
            m_current = m_max;
            UpdateBars();
        }

        public void ResumeTimer()
        {
            if (m_paused)
            {
                m_paused = false;
            }
        }

        public IEnumerator UpdateOnInterval()
        {
            while (m_running)
            {
                yield return new WaitForSeconds(tickInterval);
                tickTrigger.SetTrigger();
                m_onTimerTick.Invoke();
            }
        }
        public IEnumerator TickCoroutine()
        {

            m_running = true;
            updateBarCoroutine = EasyResourceManager.Instance.StartCoroutine(UpdateOnInterval());
            if (reverse)
                m_current = m_min;
            else
                m_current = m_max;
            while (!reverse && m_current > m_min && m_running || reverse && m_current < m_max && m_running)
            {
                if (!m_paused)
                {
                    if (reverse)
                        m_current += Time.deltaTime;
                    else
                        m_current -= Time.deltaTime;
                    if (useTimerText)
                    {
                        customString = m_current.ToString("#.00") + "s";
                    }
                    UpdateBars();

                }
                yield return null;
            }

            m_running = false;
            OnTimerEnd();
            EasyResourceManager.Instance.StopCoroutine(updateBarCoroutine);
        }

        public void OnTimerStart()
        {
            m_onTimerStart.Invoke();
            startTrigger.SetTrigger();
        }
        public void OnTimerEnd()
        {
            m_onTimerEnd.Invoke();
            endTrigger.SetTrigger();
            if (isLoop)
            {
                RestartTimer();
            }

        }

        public bool Start()
        {
            if (startTrigger.AcivateTrigger())
            {
                return true;
            }
            return false;
        }
        public bool Tick()
        {
            if (tickTrigger.AcivateTrigger())
            {
                return true;
            }
            return false;
        }
        public bool End()
        {
            if (endTrigger.AcivateTrigger())
            {
                return true;
            }
            return false;
        }

        public void PauseTimer()
        {
            if (!m_paused)
            {
                m_paused = true;
            }
        }
    }

}
