﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EasyResources
{
    public class Trigger
    {
        public bool triggered;

        public Coroutine waitCoroutine;

        public void SetTrigger()
        {
            triggered = true;
        }

        public bool AcivateTrigger()
        {
            if (triggered == true)
            {
                if(waitCoroutine == null)
                {
                    waitCoroutine = EasyResourceManager.Instance.StartCoroutine(WaitForEnd());
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public IEnumerator WaitForEnd()
        {
            yield return new WaitForEndOfFrame();
            triggered = false;
            waitCoroutine = null;
        }

    }
}
