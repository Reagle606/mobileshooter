﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }
    public bool debugMode;
    public float m_adjustSpeed;

    public Animator m_endGameAnimator;
    public Animator m_deathAnimator;

    public void PlayerDied()
    {
        m_deathAnimator.enabled = true;
        StartCoroutine(RestartAfterDelay(5));
    }
    public void EndGame()
    {
        m_endGameAnimator.enabled = true;
        StartCoroutine(RestartAfterDelay(8));
    }
    public IEnumerator RestartAfterDelay(float a_delay)
    {
        yield return new WaitForSecondsRealtime(a_delay);
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02F * Time.timeScale;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void AdjustTime(float a_time)
    {
        StartCoroutine(AdjustTimeRoutine(a_time));
    }
    public IEnumerator AdjustTimeRoutine(float a_time)
    {
        float timer = 0;
        float timeScaleStart = Time.timeScale;
        while(timer < m_adjustSpeed)
        {
            timer += Time.unscaledDeltaTime;
            Time.timeScale = Mathf.Lerp(timeScaleStart, a_time, timer / m_adjustSpeed);
            Time.fixedDeltaTime = 0.02F * Time.timeScale;
            yield return null;
        }
    }

    public void ComponentNullCheck(Component a_component, string a_componentName, GameObject a_objectActtached, bool a_autoAssigned = false)
    {
        if (debugMode)
        {
            if (a_component == null)
            {
                if (a_autoAssigned)
                {
                    Debug.LogError(a_componentName + " was not found on " + a_objectActtached + " please make sure that it is attached");
                }
            }
        }
    }

}


