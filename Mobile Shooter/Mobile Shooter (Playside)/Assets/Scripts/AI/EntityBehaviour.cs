﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EntityBehaviour 
{
    public Entity m_entity;

    public EntityBehaviour(Entity a_entity)
    {
        m_entity = a_entity;
        Start();
    }
  
    public virtual void Start()
    {

    }
    public virtual void Update()
    {

    }
}


public class CombatBehaviour : EntityBehaviour
{
    public CombatBehaviour(Entity a_entity) : base(a_entity)
    {
        
    }

    public float reactionTimer;

    public bool reacted;
    float timer = 0;
    float currentBurst;
    bool burst;

    
    bool runToCover;
    bool destinationReached = true;
    Vector3 currentDestination;

    private Transform m_currentTargetCoverPoint;
    bool canSeeTarget;
    public override void Start()
    {
        timer = Random.Range(0.6f, 2);
        currentBurst = Random.Range(1, 4);
        reactionTimer = Random.Range(0.1f, 0.2f);
        int diceRoll = Random.Range(0, 100);
        if (diceRoll > 40)
        {
            runToCover = true;
            destinationReached = false;
            m_currentTargetCoverPoint = m_entity.CurrentRoom.m_coverPoints[Random.Range(0, m_entity.CurrentRoom.m_coverPoints.Count)];
        }
        currentDestination = m_entity.transform.position;
    }
    public override void Update()
    {
        if (m_entity != null)
        {
            if (!Physics.Raycast(m_entity.transform.position + new Vector3(0, 1.6f, 0), m_entity.transform.forward, Vector3.Distance(m_entity.transform.position, m_entity.CurrentTargetEntity.transform.position), m_entity.m_obstacleMask))
            {
                canSeeTarget = true;
            }
            else
            {
                canSeeTarget = false;
            }
            if (reacted)
            {
                if (runToCover)
                {
                    if (!destinationReached)
                    {
                        m_entity.RotateTowards(m_currentTargetCoverPoint.position);

                        currentDestination = m_currentTargetCoverPoint.position;
                        m_entity.SetDestination(currentDestination);
                    }
                }
                else
                {
                    m_entity.RotateTowards(m_entity.CurrentTargetEntity.transform.position);
                    timer -= Time.deltaTime;
                    if (timer <= 0)
                    {
                        timer = Random.Range(0.7f, 2f);
                        currentBurst = Random.Range(1, 2);
                        burst = true;

                    }
                    if (m_entity.m_currentWeapon.shotsFiredInBurst < currentBurst && burst && destinationReached && canSeeTarget)
                    {
                        m_entity.m_currentWeapon.Fire();
                    }
                    else
                    {
                        if (burst == true)
                        {
                            //currentDestination = m_entity.transform.position + new Vector3(Random.insideUnitSphere.x * 4, 0, Random.insideUnitSphere.z * 4);
                            //m_entity.SetDestination(currentDestination);
                            m_entity.m_currentWeapon.shotsFiredInBurst = 0;
                            burst = false;

                        }


                    }

                }
                if (Vector3.Distance(m_entity.transform.position, currentDestination) < 0.4f)
                {
                    destinationReached = true;
                    runToCover = false;
                }
                else
                {
                    destinationReached = false;
                }
            }
            else
            {
                reactionTimer -= Time.deltaTime;
                if (reactionTimer <= 0)
                {
                    reacted = true;
                }
            }
        }
    }

    
}


