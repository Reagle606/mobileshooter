﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attachment : ScriptableObject
{

    public AttachementObject m_attachmentObject;
    public AttachementObject m_attachmentObjectPrefab;

    public virtual Attachment GetCopy()
    {
        Attachment i = ScriptableObject.CreateInstance<Attachment>();
        i.Initialise(this);
        return i;
    }
    //Initialises all the needed variables for the copied Objct
    public virtual void Initialise(Attachment a_other)
    {
        m_attachmentObjectPrefab = a_other.m_attachmentObjectPrefab;
    }
}
