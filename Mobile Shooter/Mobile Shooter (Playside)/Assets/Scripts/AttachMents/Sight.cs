﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Weapons/New Sight")]
public class Sight : Attachment
{
    public Vector3 m_aimedPos;
    public Vector3 m_aimedRot;
    public int m_zoomAmount;


    public override Attachment GetCopy()
    {
        Sight i = ScriptableObject.CreateInstance<Sight>();
        i.Initialise(this);
        return i;
    }
    //Initialises all the needed variables for the copied Objct
    public override void Initialise(Attachment a_other)
    {
        base.Initialise(a_other);
        m_aimedPos = ((Sight)a_other).m_aimedPos;
        m_aimedRot = ((Sight)a_other).m_aimedRot;
        m_zoomAmount = ((Sight)a_other).m_zoomAmount;
    }
}
