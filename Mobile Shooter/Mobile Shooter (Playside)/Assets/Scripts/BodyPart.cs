﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyPart : MonoBehaviour
{
    public enum BodyPartType
    {
        Head,
        Torso,
        Leg
    }
    public BodyPartType m_bodyPartType;

}
