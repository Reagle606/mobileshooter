﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreachManager : MonoBehaviour , IInteractable
{
    public Entity m_breacher;
    public Animator m_animator;

    public Transform m_characterTransform;

    public bool canBreach;
    public bool breached;
    public bool finalDoor;

    float timer = 0;

    public void Update()
    {
        if (breached)
        {
            timer += Time.unscaledDeltaTime;
            if (timer > 3)
            {
                GameManager.Instance.AdjustTime(1);
                breached = false;
            }
        }
    }

    public void StartRoom()
    {
        LevelManager.Instance.StartNextRoom();
    }
  
   
    public void InitialiseBreach(Entity a_entity)
    {
        if (canBreach)
        {
            if(finalDoor)
            {
                GameManager.Instance.EndGame();
            }
            m_breacher = a_entity;
            GetComponent<Collider>().enabled = false;
            m_breacher.GetComponent<Rigidbody>().isKinematic = true;
            m_breacher = a_entity;
            m_breacher.transform.SetParent(m_characterTransform);
            m_breacher.GetComponent<EntityMovement>().enabled = false;
            m_breacher.Animator.SetTrigger("Breach");
            m_animator.SetTrigger("Breach");
            m_breacher.transform.localPosition = Vector3.zero;
            m_breacher.transform.localRotation = Quaternion.identity;
            m_breacher.GetComponentInChildren<SmoothLook>().enabled = false;

            m_breacher.GetComponent<EntityInput>().enabled = false;
            m_breacher.EnableShooting(false);
        }
        
    }
    
    public void SlowTime()
    {
        GameManager.Instance.AdjustTime(0.25f);
        breached = true;
    }
    public void EndBreach()
    {
        m_breacher.transform.SetParent(null);
        m_breacher.transform.rotation = m_characterTransform.transform.rotation;
        m_breacher.GetComponent<Rigidbody>().isKinematic = false;
        m_breacher.GetComponent<EntityMovement>().enabled = true;
        m_breacher.EnableShooting(true);
        m_breacher.GetComponent<EntityInput>().enabled = true;
        m_breacher.GetComponentInChildren<SmoothLook>().enabled = true;
    }

    void IInteractable.Interact(Entity a_interactor)
    {
        InitialiseBreach(a_interactor);
    }
}
