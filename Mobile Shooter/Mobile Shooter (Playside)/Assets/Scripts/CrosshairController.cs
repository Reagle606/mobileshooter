﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrosshairController : MonoBehaviour
{
    public static CrosshairController Instance;

    public void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public Entity m_entity;

    public GameObject m_crossHair;
    public List<GameObject> m_hairs;

    public void Update()
    {
        if (m_entity.m_currentWeapon != null)
        {
            foreach (GameObject h in m_hairs)
            {
                h.transform.localPosition = h.transform.right * m_entity.m_currentWeapon.m_currentAccuracy * 75;
            }
        }
    }
}
