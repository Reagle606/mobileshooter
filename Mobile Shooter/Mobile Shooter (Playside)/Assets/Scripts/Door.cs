﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public Transform m_doorHinge;
    public float doorOpenTime = 1;

    public Coroutine m_doorOpenCoroutine;
    public void OpenDoor()
    {
        if(m_doorOpenCoroutine != null)
        {
          
        }
        m_doorOpenCoroutine = StartCoroutine(DoorOpenRoutine());
    }
    public IEnumerator DoorOpenRoutine()
    {
        float timer = 0;

        Vector3 endAngle = m_doorHinge.transform.eulerAngles + new Vector3(0, 90, 0);
      
        while (timer < doorOpenTime)
        {
            timer += Time.deltaTime;
            m_doorHinge.transform.eulerAngles = Vector3.Lerp(m_doorHinge.transform.eulerAngles, endAngle, timer / doorOpenTime);
           
            yield return null;
        }

    }
}
