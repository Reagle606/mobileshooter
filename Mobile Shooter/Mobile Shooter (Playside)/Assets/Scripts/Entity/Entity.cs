﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyResources;
using UnityEngine.AI;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(NavMeshAgent))]
public class Entity : MonoBehaviour, IDamagable
{
    public bool m_playerControlled;

    public int m_maxHealth = 100;
    public Resource m_health;

    public List<Magazine> m_magazines = new List<Magazine>();

    public List<Weapon> m_heldWeapons = new List<Weapon>();

    [HideInInspector]
    public Weapon m_currentWeapon;

    [Header("Transforms")]
    public Transform m_camera;
    //Where the character grips the weapon
    public Transform m_weaponHoldPos;
    //the actual character model of the entity
    public Transform m_graphics;
    //the aimer of the entity used for ads
    public Transform m_aimHolder;

    [Header("Prefab References")]
    public GameObject m_bulletImpactPrefab;
    public TMPro.TextMeshProUGUI m_interactionText;

    [Header("LayerMasks")]
    public LayerMask m_obstacleMask;
    public LayerMask m_interactionMask;

    private EntityBehaviour m_currentBehaviour;

    private bool m_aiming;
    public bool Aiming { get { return m_aiming; } }

    private Entity m_currentTargetEnemy;
    public Entity CurrentTargetEntity { get { return m_currentTargetEnemy; } }


    private Room m_currentRoom;
    public Room CurrentRoom { get { return m_currentRoom; } set { m_currentRoom = value; } }

    #region References
    private EntityMovement m_movement;
    public EntityMovement Movement { get { return m_movement; } }

    private NavMeshAgent m_navMeshAgent;
    public NavMeshAgent NavMeshAgent { get { return m_navMeshAgent; } }

    private Animator m_animator;
    public Animator Animator { get { return m_animator; } }

    private SmoothLook m_smoothLook;
    public SmoothLook SmoothLook { get { return m_smoothLook; } }

    private RagdollManager m_ragdollManager;
    public RagdollManager RagdollManager { get { return m_ragdollManager; } }
    #endregion

    //Coroutines for actions stored
    private Coroutine m_switchWeaponCoroutine;
    private Coroutine m_reloadCoroutine;
    private Coroutine m_cockWeaponCoroutine;
    private Coroutine m_aimDownSightCoroutine;
    private Coroutine m_healthRegenCoroutine;


    private FixedButton m_interactButton;

    //list of things locking the player from firing, only used a byte because its the smallest amount of data
    private List<byte> m_fireLocks = new List<byte>();

    private bool initialised;

    private int m_currentWeaponIndex = 0;
    private Timer m_damageRegenTimer;
    public float m_healthRegenRate = 2;

    private IInteractable m_interactable;

    public bool isDead;
    public Transform m_magUiHolder;
    public Animator m_hitMarker;
    public void Start()
    {
        Initialise();
    }
    public void Initialise()
    {
        initialised = false;
        //auto assigns all components needed and checks them
        m_navMeshAgent = GetComponent<NavMeshAgent>();
        m_movement = GetComponent<EntityMovement>();
        m_animator = GetComponent<Animator>();
        m_smoothLook = GetComponentInChildren<SmoothLook>();
        m_ragdollManager = GetComponent<RagdollManager>();

        GameManager.Instance.ComponentNullCheck(m_navMeshAgent, "NavMeshAgent", gameObject, true);
        GameManager.Instance.ComponentNullCheck(m_movement, "EntityMovement", gameObject, true);
        GameManager.Instance.ComponentNullCheck(m_animator, "Animator", gameObject, true);
        GameManager.Instance.ComponentNullCheck(m_smoothLook, "Smooth Look", gameObject, true);
        m_interactButton = GetComponent<EntityInput>().m_interactButton;

        if (m_camera == null)
            m_camera = transform.FindDeepChild("Main Camera");
        if (m_graphics == null)
            m_graphics = transform.Find("Graphic");

        m_damageRegenTimer = new Timer(3);

        //Initialised any mags and weapons set in editor
        for (int i = 0; i < m_heldWeapons.Count; i++)
        {
            if (m_heldWeapons[i] != null)
            {
                m_heldWeapons[i] = m_heldWeapons[i].GetCopy();
            }
        }
        for (int i = 0; i < m_magazines.Count; i++)
        {
            m_magazines[i] = m_magazines[i].GetCopy();
        }
        //Sets up enetites that are nit player controlled
        if (!m_playerControlled)
        {
            GetComponent<EntityInput>().enabled = false;
            m_camera.gameObject.SetActive(false);
            GetComponentInChildren<SmoothLook>().enabled = false;
            m_currentBehaviour = new CombatBehaviour(this);
            m_currentTargetEnemy = GameObject.FindWithTag("Player").GetComponent<Entity>();
            GetComponent<Rigidbody>().isKinematic = true;
            m_navMeshAgent.enabled = true;
        }
        else
        {
            m_graphics.gameObject.SetActive(false);
        }

        m_switchWeaponCoroutine = null;
        m_reloadCoroutine = null;

        SwitchWeapon();
        m_health.m_max = m_maxHealth;
        m_health.m_current = m_maxHealth;
        initialised = true;
    }
    public void Update()
    {
        if (initialised)
        {
            if (m_currentWeapon != null)
            {
                if (m_currentWeapon.m_currentAccuracy > m_currentWeapon.m_accuracy)
                {
                    m_currentWeapon.m_currentAccuracy -= m_currentWeapon.m_accuracyRegain * Time.deltaTime;
                }
            }
            if (m_playerControlled)
                ItemScan();
            else if (m_currentBehaviour != null)
            {
                {
                    m_currentBehaviour.Update();
                }
            }

            if (m_damageRegenTimer.End())
            {
                m_healthRegenCoroutine = StartCoroutine(RegenHealthRoutine());
            }
        }
    }

    public IEnumerator RegenHealthRoutine()
    {
        float timer = 0;
        float adjustment;
        float start = m_health.m_current;
        float end = m_health.m_max;
        while (timer < m_healthRegenRate)
        {
            timer += Time.deltaTime;
            adjustment = Mathf.Lerp(start, end, timer / m_healthRegenRate);
            m_health.SetValue(adjustment);
            yield return null;
        }
        m_healthRegenCoroutine = null;
    }
    public void CancelHealthRegen()
    {
        if (m_healthRegenCoroutine != null)
        {
            StopCoroutine(m_healthRegenCoroutine);
        }
    }

    public void Interact()
    {
        if (m_interactable != null)
        {
            m_interactButton.Pressed = false;
            m_interactable.Interact(this);
        }
    }
    //scans for interactables
    public void ItemScan()
    {
        RaycastHit hit;
        if (Physics.Raycast(m_camera.transform.position, m_camera.transform.forward, out hit, 2, m_interactionMask))
        {
            m_interactable = hit.collider.GetComponent<IInteractable>();
            if (m_interactable != null)
            {
                m_interactionText.text = hit.collider.name;
                if (!m_interactButton.isActiveAndEnabled)
                {
                    m_interactButton.gameObject.SetActive(true);
                }
                m_interactButton.transform.position = Camera.main.WorldToScreenPoint(hit.collider.transform.position);

            }
            else
            {
                if (m_interactButton.isActiveAndEnabled)
                {
                    m_interactButton.gameObject.SetActive(false);
                }
                m_interactionText.text = "";
            }
        }
        else
        {
            if (m_interactButton.isActiveAndEnabled)
            {
                m_interactButton.gameObject.SetActive(false);
            }
            m_interactable = null;
            m_interactionText.text = "";
        }


    }

    //This adds a lock or removes a lock from firing if all locks are removed the player may fire, 
    //this prevents certan areas setting "CanFire" to true when you want it to stay false whilst doing an action
    public void EnableShooting(bool value)
    {
        if (!value)
            m_fireLocks.Add(new byte());
        else if (m_fireLocks.Count > 0)
        {
            m_fireLocks.Remove(m_fireLocks[0]);
        }
    }

    //Is the entity locked from firing thier weapon
    public bool IsFireLocked()
    {
        if (m_fireLocks.Count > 0)
        {
            return true;
        }
        return false;
    }

    private void UpdateAmmoBar()
    {
        if (m_currentWeapon.m_loadedMagazine != null)
        {
            if (!m_currentWeapon.m_loadedMagazine.m_ammo.bars.Contains(UIMananger.Instance.ammoBar))
            {
                m_currentWeapon.m_loadedMagazine.m_ammo.bars.Add(UIMananger.Instance.ammoBar);
            }
            m_currentWeapon.m_loadedMagazine.m_ammo.UpdateBars();
        }
    }

    #region WeaponSwitching
    public void SwitchWeapon()
    {
        if (m_switchWeaponCoroutine == null && m_cockWeaponCoroutine == null && m_reloadCoroutine == null)
        {
            int a_index = m_currentWeaponIndex++;
            if (a_index > m_heldWeapons.Count - 1)
            {
                a_index = 0;
            }
            //Cancel switch weapon
            if (m_heldWeapons[a_index] != null)
            {
                CancelSwitchWeapon();
                if (m_heldWeapons.Count > 0 && a_index <= m_heldWeapons.Count)
                {
                    if (m_heldWeapons[a_index] != m_currentWeapon)
                    {
                        m_switchWeaponCoroutine = StartCoroutine(SwitchWeaponRoutine(m_heldWeapons[a_index]));
                        m_currentWeaponIndex = a_index;
                    }
                }
            }
        }
    }
    private IEnumerator SwitchWeaponRoutine(Weapon a_newWeapon)
    {
        EnableShooting(false);
        CancelReload();
        if (m_currentWeapon != null)
        {
            m_animator.ResetTrigger("DrawWeapon");
            m_animator.SetTrigger("SwapWeapon");
            yield return new WaitForSeconds(m_currentWeapon.m_sheatheTime);
            m_animator.ResetTrigger("SwapWeapon");
            m_currentWeapon.m_weaponObject.gameObject.SetActive(false);
        }
      

        RefreshAmmoUI(a_newWeapon);


        switch (a_newWeapon.m_ammoType)
        {
            case Magazine.AmmoType.AR:
                m_animator.SetLayerWeight(1, 1);
                m_animator.SetLayerWeight(2, 0);
                break;
            case Magazine.AmmoType.Pistol:

                break;
            case Magazine.AmmoType.SMG:

                break;
            case Magazine.AmmoType.ShotGun:
                m_animator.SetLayerWeight(1, 0);
                m_animator.SetLayerWeight(2, 1);
                break;
        }

        m_currentWeapon = a_newWeapon;
        if (m_playerControlled)
            UpdateAmmoBar();
        if (m_currentWeapon.m_weaponObject == null)
        {
            WeaponObject weaponObject = Instantiate(m_currentWeapon.m_weaponObjectPrefab, m_weaponHoldPos.transform.position, m_weaponHoldPos.transform.rotation, m_weaponHoldPos);

            weaponObject.m_weapon = m_currentWeapon;
            weaponObject.Initialise();
            weaponObject.m_rigidBody.isKinematic = true;
            weaponObject.m_collider.enabled = false;
            m_currentWeapon.m_weaponObject = weaponObject;
            if (m_currentWeapon.m_currentSight == null)
            {
                m_currentWeapon.Attach(m_currentWeapon.m_defaultSight);
            }
        }
        else
        {
            m_currentWeapon.m_weaponObject.gameObject.SetActive(true);
            m_currentWeapon.m_weaponObject.transform.SetParent(m_weaponHoldPos);
            m_currentWeapon.m_weaponObject.transform.localPosition = Vector3.zero;
            m_currentWeapon.m_weaponObject.transform.localRotation = Quaternion.identity;
            m_currentWeapon.m_weaponObject.m_rigidBody.isKinematic = true;
            m_currentWeapon.m_weaponObject.m_collider.enabled = false;
        }
        m_currentWeapon.SetHolder(this);
        m_animator.SetTrigger("DrawWeapon");
        yield return new WaitForSeconds(m_currentWeapon.m_drawTime);
        if (!m_currentWeapon.m_cocked)
        {
            CockWeapon(0, true);
        }
        EnableShooting(true);
        m_switchWeaponCoroutine = null;
    }
    private void CancelSwitchWeapon()
    {
        if (m_switchWeaponCoroutine != null)
        {
            StopCoroutine(m_switchWeaponCoroutine);
            EnableShooting(true);
        }
    }

    public void RefreshAmmoUI(Weapon a_newWeapon)
    {
        if (m_playerControlled)
        {
            for (int i = 0; i < m_magUiHolder.childCount; i++)
            {
                Destroy(m_magUiHolder.transform.GetChild(i).gameObject);
            }

            float ammoUIAmount = 1;
            foreach (Magazine m in m_magazines)
            {
                if (m.m_ammoType == a_newWeapon.m_ammoType)
                {
                    if (a_newWeapon.m_internalMag)
                    {
                        ammoUIAmount = m.m_ammo.m_current;
                    }
                    for (int i = 0; i < ammoUIAmount; i++)
                    {
                        Image im = Instantiate(new GameObject(), m_magUiHolder).AddComponent<Image>();
                        im.sprite = a_newWeapon.m_magSprite;
                    }
                }
            }
        }
    }

    public void DropWeapon()
    {
        if (m_currentWeapon != null)
        {
            m_currentWeapon.m_weaponObject.transform.SetParent(null);
            m_currentWeapon.m_weaponObject.m_collider.enabled = true;
            m_currentWeapon.m_weaponObject.m_rigidBody.isKinematic = false;
            m_currentWeapon = null;
        }

    }
    public void PickupWeapon(Weapon a_weapon)
    {
        m_heldWeapons[m_currentWeaponIndex] = a_weapon;
        SwitchWeapon();
    }
    #endregion

    #region WeaponAiming
    public void AimDownSights(bool value)
    {
        if (m_aimDownSightCoroutine == null)
        {
            m_aimDownSightCoroutine = StartCoroutine(AimDownSightsRoutine(value));
        }
    }
    public IEnumerator AimDownSightsRoutine(bool value)
    {
        float timer = 0;
        Vector3 endPos = Vector3.zero;
        Vector3 endRot = Vector3.zero;
        Vector3 startPos = m_aimHolder.transform.localPosition;
        Vector3 startRot = m_aimHolder.transform.localEulerAngles;
        float startAim = 0;
        float endAim = 0;
        float aim = 0;
        int startFov = 0;
        int endFov = 0;
        if (value)
        {
            endPos = m_currentWeapon.m_currentSight.m_aimedPos;
            endRot = m_currentWeapon.m_currentSight.m_aimedRot;
            GetComponentInChildren<WeaponSway>().m_amount = GetComponentInChildren<WeaponSway>().m_aimingSway;
            startAim = 0;
            endAim = 1;
            startFov = 70;
            endFov = 70 - m_currentWeapon.m_currentSight.m_zoomAmount;
        }
        else
        {
            GetComponentInChildren<WeaponSway>().m_amount = GetComponentInChildren<WeaponSway>().m_defaultSway;
            startAim = 1;
            endAim = 0;
            startFov = 70 - m_currentWeapon.m_currentSight.m_zoomAmount;
            endFov = 70;
        }

        while (timer < m_currentWeapon.m_aimDownSightSpeed)
        {
            timer += Time.deltaTime;
            m_aimHolder.transform.localPosition = Vector3.Lerp(startPos, endPos, timer / m_currentWeapon.m_aimDownSightSpeed);
            m_aimHolder.transform.localRotation = Quaternion.Lerp(Quaternion.Euler(startRot), Quaternion.Euler(endRot), timer / m_currentWeapon.m_aimDownSightSpeed);
            aim = Mathf.Lerp(startAim, endAim, timer / m_currentWeapon.m_aimDownSightSpeed);
            m_camera.GetComponent<Camera>().fieldOfView = Mathf.Lerp(startFov, endFov, timer / m_currentWeapon.m_aimDownSightSpeed);
            m_animator.SetFloat("Aimed", aim);
            yield return null;
        }
        CrosshairController.Instance.m_crossHair.SetActive(!value);
        if (value)
        {
            m_currentWeapon.m_currentAccuracy = m_currentWeapon.m_aimAccuracy;
        }
        else
        {
            m_currentWeapon.m_currentAccuracy = m_currentWeapon.m_accuracy;

        }

        m_aiming = value;
        m_aimDownSightCoroutine = null;
    }
    public void CancelAimDownSights()
    {
        if (m_aimDownSightCoroutine != null)
        {
            StopCoroutine(m_aimDownSightCoroutine);
            m_aimDownSightCoroutine = null;
        }
    }
    #endregion

    #region WeaponReloading

    public void CockWeapon(float a_delay, bool a_manual)
    {
        if (m_cockWeaponCoroutine == null)
        {
            m_cockWeaponCoroutine = StartCoroutine(CockWeaponRoutine(a_delay, a_manual));
        }
    }
    private IEnumerator CockWeaponRoutine(float a_delay, bool a_manual)
    {
        yield return new WaitForSeconds(a_delay);
        if (a_manual)
        {
            m_animator.SetTrigger("CockWeapon");
            EnableShooting(false);

        }
        yield return new WaitForSeconds(m_currentWeapon.m_cockingTime);
        if (a_manual)
        {
            EnableShooting(true);
        }
        if (m_currentWeapon != null)
        {
            m_currentWeapon.m_cocked = true;
        }
        m_cockWeaponCoroutine = null;
    }

    public void ReloadWeapon()
    {
        if (m_magazines.Count > 0 && m_reloadCoroutine == null && m_currentWeapon.m_loadedMagazine?.m_ammo.m_current != m_currentWeapon.m_loadedMagazine?.m_ammo.m_max)
        {
            Magazine magtoLoad = null;
            foreach (Magazine mag in m_magazines)
            {
                if (mag.m_ammoType == m_currentWeapon.m_ammoType)
                {
                    magtoLoad = mag;
                    continue;
                }
            }
            if (magtoLoad != null)
            {
                if (m_currentWeapon.m_internalMag)
                {
                    m_reloadCoroutine = StartCoroutine(ReloadWeaponInternalMag(magtoLoad));
                }
                else
                {
                    m_reloadCoroutine = StartCoroutine(ReloadWeaponRoutine(magtoLoad));
                }

            }
        }

    }
    private IEnumerator ReloadWeaponRoutine(Magazine a_magToLoad)
    {
        m_animator.SetTrigger("Reload");
        EnableShooting(false);

        yield return new WaitForSeconds(m_currentWeapon.m_reloadTime);
        if (!m_currentWeapon.m_loadedMagazine.m_ammo.isEmpty)
        {
            m_magazines.Add(m_currentWeapon.m_loadedMagazine);
        }
       
        m_currentWeapon.UnloadMagazine();
        m_currentWeapon.LoadMagazine(a_magToLoad);
        m_magazines.Remove(a_magToLoad);
        RefreshAmmoUI(m_currentWeapon);
        UpdateAmmoBar();
        EnableShooting(true);
        m_reloadCoroutine = null;
    }
    private IEnumerator ReloadWeaponInternalMag(Magazine a_ammoToUse)
    {
        m_animator.SetTrigger("Reload");
        EnableShooting(false);

        yield return new WaitForSeconds(m_currentWeapon.m_reloadTime);
        a_ammoToUse.m_ammo.AdjustValue(-1);
        if (a_ammoToUse.m_ammo.isEmpty)
        {
            m_magazines.Remove(a_ammoToUse);
        }
        RefreshAmmoUI(m_currentWeapon);
        m_currentWeapon.LoadBullet();
        UpdateAmmoBar();
        EnableShooting(true);
        m_reloadCoroutine = null;
    }
    private void CancelReload()
    {
        if (m_reloadCoroutine != null)
        {
            StopCoroutine(m_reloadCoroutine);
            EnableShooting(true);
        }

    }
    #endregion

    #region Damaging
    void IDamagable.TakeDamage(float a_kineticDamage, float a_energyDamage, Vector3 a_hitPoint, Vector3 a_hitNormal, Rigidbody hitBone, Entity a_damager)
    {
        if (a_damager != this)
        {
            m_damageRegenTimer.ResetTimer();
            m_damageRegenTimer.StartTimer();
            CancelHealthRegen();
            m_health.AdjustValue(-(a_kineticDamage + a_energyDamage));
            GameObject impact = Instantiate(m_bulletImpactPrefab, a_hitPoint, Quaternion.identity);
            impact.transform.forward = a_hitNormal;
            Destroy(impact, 2);
        }
        if (m_health.m_current <= 0)
        {
            Die(a_hitPoint, a_hitNormal, hitBone);
        }
    }
    public void Die(Vector3 a_hitPoint, Vector3 a_hitNormal, Rigidbody hitBone)
    {


        m_ragdollManager.EnableRagdoll(a_hitPoint, a_hitNormal, hitBone);
        if (!m_playerControlled)
        {
            LevelManager.Instance.UpdateRoom(this);
            DropWeapon();
            GetComponent<CapsuleCollider>().enabled = false;
            Destroy(this.gameObject, 6);
        }
        else
        {
            GameManager.Instance.PlayerDied();
            this.enabled = false;
            SmoothLook.enabled = false;
        }
        isDead = true;
        m_animator.enabled = false;
        this.enabled = false;
    }
    #endregion

    #region AI
    public void RotateTowards(Vector3 a_target)
    {
        Quaternion lookOnLook = Quaternion.LookRotation(a_target - transform.position);

        transform.rotation = Quaternion.Lerp(transform.rotation, lookOnLook, Time.deltaTime * 7.5f);
    }

    public void SetDestination(Vector3 a_position)
    {
        if (m_navMeshAgent.isActiveAndEnabled)
            m_navMeshAgent.SetDestination(a_position);
    }
    #endregion
}
