﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityInput : MonoBehaviour
{
    private Entity m_entity;

    public FixedButton m_swapWeaponButton;
    public FixedTouchPad m_moveTouchPad;
    public FixedButton m_interactButton;
    public FixedButton m_fireButton1, m_firedButton2;
    public FixedButton m_aimButton;
    public FixedButton m_reloadButton;
    public int m_movementSensitivity = 70;
    public bool pcDebug;
    public void Start()
    {
        m_entity = GetComponent<Entity>();
    }

    public void Update()
    {
        WeaponInput();
        MoveInput();
    }

    public void MoveInput()
    {

       
            m_entity.Movement.MoveInput = new Vector3(m_moveTouchPad.TouchDist.x / m_movementSensitivity, 0, m_moveTouchPad.TouchDist.y / m_movementSensitivity);
            m_entity.Movement.MoveInput.x = Mathf.Clamp(m_entity.Movement.MoveInput.x, -1, 1);
            m_entity.Movement.MoveInput.z = Mathf.Clamp(m_entity.Movement.MoveInput.z, -1, 1);
        
        m_entity.Animator.SetFloat("Horizontal", m_entity.Movement.MoveInput.x);
        m_entity.Animator.SetFloat("Vertical", m_entity.Movement.MoveInput.y);
    }
    public void WeaponInput()
    {
        if (pcDebug)
        {
            if (Input.GetButtonDown("Primary Fire"))
            {
                m_entity.m_currentWeapon.Fire();
            }
            if (Input.GetButtonDown("Secondary Fire"))
            {
                m_entity.AimDownSights(!m_entity.Aiming);
            }
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                m_entity.SwitchWeapon();
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                m_entity.SwitchWeapon();
            }
        }
        else
        {
            if (m_fireButton1.Pressed || m_firedButton2.Pressed)
            {
                m_entity.m_currentWeapon.Fire();
            }
            if (m_aimButton.Pressed)
            {
                m_entity.AimDownSights(!m_entity.Aiming);
            }
            if (m_reloadButton.Pressed)
            {
                m_entity.ReloadWeapon();
            }
            if(m_interactButton.Pressed)
            {
                m_entity.Interact();
            }
            if(m_swapWeaponButton.Pressed)
            {
                m_entity.SwitchWeapon();
            }
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            GameManager.Instance.AdjustTime(0.3f);
           
        }


    

      
    }

}
