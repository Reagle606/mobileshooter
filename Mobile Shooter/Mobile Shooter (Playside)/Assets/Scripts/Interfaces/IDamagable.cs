﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamagable
{
    void TakeDamage(float a_kineticDamage, float a_energyDamage,Vector3 a_hitPoint, Vector3 a_hitNormal, Rigidbody hitBone = null, Entity a_damager = null);
    void Die(Vector3 a_hitPoint, Vector3 a_hitNormal, Rigidbody hitBone);
}
