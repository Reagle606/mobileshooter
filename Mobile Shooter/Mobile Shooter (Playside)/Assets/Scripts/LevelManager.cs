﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;

    public Entity m_enemyPrefab;

    public Room[] m_rooms;
    public Room m_currentRoom;
    public int m_roomIndex = -1;
    

    public void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        m_rooms = GetComponentsInChildren<Room>();

        m_roomIndex = -1;
    }

    public void UpdateRoom(Entity a_entity)
    {
        if (m_currentRoom != null)
        {
            m_currentRoom.UpdateRoom(a_entity);
        }
    }


    public void StartNextRoom()
    {
        m_roomIndex++;
        if(m_roomIndex < m_rooms.Length)
        {
            m_currentRoom = m_rooms[m_roomIndex];
            StartRoom(m_currentRoom);
        }
    }
    public void StartRoom(Room a_room)
    {
        a_room.OnSpawn();
    }
}
