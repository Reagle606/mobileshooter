﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollManager : MonoBehaviour
{
    public Rigidbody[] m_rigidbodies;
    public void Start()
    {
        SetupRagdoll();
    }
    public void EnableRagdoll(Vector3 hitLocation, Vector3 hitNormal, Rigidbody hitBone = null)
    {
        foreach (Rigidbody rigid in m_rigidbodies)
        {
            if (rigid != m_rigidbodies[0])
            {
                rigid.isKinematic = false;
                rigid.useGravity = true;
                if (rigid == hitBone)
                {
                    rigid.AddForce(-hitNormal * 100, ForceMode.Impulse);
                }
                if (rigid.GetComponent<Collider>().enabled == false)
                {
                    rigid.GetComponent<Collider>().enabled = true;
                }

            }

        }

    }

    public void SetupRagdoll()
    {
        Rigidbody[] r = GetComponentsInChildren<Rigidbody>();
        if(!GetComponent<Entity>().m_playerControlled)
        {
            GetComponent<CapsuleCollider>().isTrigger = true;
        }
        
        foreach (Rigidbody rigid in r)
        {
            if (rigid != r[0])
            {
                rigid.isKinematic = true;
                rigid.useGravity = false;
                rigid.gameObject.layer = LayerMask.NameToLayer("RagdollPart");
                if (!rigid.GetComponent<BodyPart>())
                {
                    rigid.GetComponent<Collider>().enabled = false;
                }
            }

        }
        m_rigidbodies = r;
    }

    
}
