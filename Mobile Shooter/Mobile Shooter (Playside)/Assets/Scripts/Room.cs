﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    public SpawnPoint[] m_spawnPoints;

    public int m_enemyCount;

    public List<Entity> m_entities;

    public List<Transform> m_coverPoints;

    public bool complete;
    public BreachManager m_exitDoor;
 
    public void GetSpawnPoints()
    {
        m_spawnPoints = GetComponentsInChildren<SpawnPoint>();
    }
    public void OnSpawn()
    {
        GetSpawnPoints();
          
        for (int i = 0; i < m_enemyCount; i++)
        {
            SpawnPoint point = m_spawnPoints[i];
            Entity e = Instantiate(LevelManager.Instance.m_enemyPrefab, point.transform.position, Quaternion.identity);
            e.CurrentRoom = this;
            m_entities.Add(e);
        }
    }
    public void UpdateRoom(Entity a_entity)
    {
        m_entities.Remove(a_entity);
        if(m_entities.Count <= 0)
        {
            complete = true;
            if (m_exitDoor != null)
            {
                m_exitDoor.canBreach = true;
            }
        }
       
    }
}
