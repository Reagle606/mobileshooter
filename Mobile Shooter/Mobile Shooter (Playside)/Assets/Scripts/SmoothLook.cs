﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SmoothLook : MonoBehaviour
{
    public bool isActive = true;

    public Vector2 clampInDegrees = new Vector2(360, 180);
    public bool lockCursor;
    public Vector2 sensitivity = new Vector2(2, 2);
    public Vector2 defaultSensitivity = new Vector2(2, 2);
    public Vector2 lockedOnSensitivity = new Vector2(2, 2);


  
    public bool pcDebug;

    public float aimAssistStength = 2.5f;
    public Vector3 recoil;
    public LayerMask m_assistMask;
    public Transform m_target;
    public GameObject characterBody;

    void Start()
    {
        sensitivity = defaultSensitivity;
        lockedOnSensitivity = defaultSensitivity / 2;
    }

    public FixedTouchPad m_touchPad;

    public void ChangeSensitivity(float a_sens)
    {
        defaultSensitivity = new Vector2(a_sens * 5, a_sens  * 5);
        lockedOnSensitivity = defaultSensitivity / 2;
    }
    void Update()
    {
        if (isActive)
        {
           

            Vector3 endHeadRot = transform.localEulerAngles;
            endHeadRot -= new Vector3(m_touchPad.TouchDist.y / 10 * sensitivity.y, 0, 0);
            //// Then clamp and apply the global y value.
            if (clampInDegrees.y < 360)
               // endHeadRot.x = Mathf.Clamp(endHeadRot.x, -clampInDegrees.y * 0.5f, clampInDegrees.y * 0.5f);

            transform.localEulerAngles = endHeadRot;
            characterBody.transform.eulerAngles += new Vector3(0, m_touchPad.TouchDist.x / 10 * sensitivity.x, 0);


            RaycastHit hit;
            Debug.DrawRay(GetComponentInParent<Entity>().m_camera.position, characterBody.transform.forward * 50);
            if (Physics.SphereCast(GetComponentInParent<Entity>().m_camera.position, 0.2f, characterBody.transform.forward, out hit, 50, m_assistMask))
            {
                m_target = hit.collider.transform;
                if (m_target != null)
                {
                
                    var targetRotation = Quaternion.LookRotation(m_target.transform.position - characterBody.transform.position);

                    // Smoothly rotate towards the target point.
                    characterBody.transform.rotation = Quaternion.Slerp(characterBody.transform.rotation, targetRotation, aimAssistStength * Time.unscaledDeltaTime);
                    sensitivity = lockedOnSensitivity;

                }
                else
                {
                    sensitivity = defaultSensitivity;
                }
            }
            else
            {
                sensitivity = defaultSensitivity;
                m_target = null;
            }
          
        }
    }
}
