﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyResources;

[CreateAssetMenu(menuName = "Weapons/New Weapon")]
public class Weapon : ScriptableObject
{
    public Magazine m_loadedMagazine;

    public int m_bulletsFiredPerShot = 1;
    public int m_ammoUsedPerShot = 1;

    public float m_drawTime, m_sheatheTime;

    public float m_kineticDamage, m_energyDamage;

    public float m_reloadTime = 1;
    public bool m_internalMag;

    public Magazine.AmmoType m_ammoType;
    public enum FireMode
    {
        FullAuto,
        SemiAuto,
        BoltAction
    }
    public FireMode m_fireMode;

    private Entity m_holder;
    public WeaponObject m_weaponObjectPrefab;

    public WeaponObject m_weaponObject;

    public float m_bulletCasingEjectForce;

    public GameObject m_bulletCasingPrefab;

    public GameObject m_defaultBulletImpactPrefab;
    public BulletTracers m_tracerPrefab;

    public float m_accuracyPerShot;
    public float m_accuracy;
    public float m_currentAccuracy;
    public float m_accuracyRegain;
    public float m_recoilVerticalPerShot;

    public float m_aimAccuracy;

    public float m_cockingTime;
    public bool m_cocked;
    public float m_aimDownSightSpeed;
    public int shotsFiredInBurst = 0;
    public Sight m_defaultSight;

    public Sight m_currentSight;

    public Sprite m_magSprite;
  
    //Returns an exact copy of the scritable object
    public virtual Weapon GetCopy()
    {
        Weapon i = ScriptableObject.CreateInstance<Weapon>();
        i.Initialise(this);
        return i;
    }
    //Initialises all the needed variables for the copied Objct
    public virtual void Initialise(Weapon a_other)
    {
        m_bulletsFiredPerShot = a_other.m_bulletsFiredPerShot;
        m_drawTime = a_other.m_drawTime;
        m_sheatheTime = a_other.m_sheatheTime;
        m_kineticDamage = a_other.m_kineticDamage;
        m_energyDamage = a_other.m_energyDamage;
        m_weaponObjectPrefab = a_other.m_weaponObjectPrefab;
        m_bulletCasingEjectForce = a_other.m_bulletCasingEjectForce;
        m_bulletCasingPrefab = a_other.m_bulletCasingPrefab;
        m_defaultBulletImpactPrefab = a_other.m_defaultBulletImpactPrefab;
        m_recoilVerticalPerShot = a_other.m_recoilVerticalPerShot;
        m_fireMode = a_other.m_fireMode;
        m_accuracy = a_other.m_accuracy;
        m_currentAccuracy = m_accuracy;
        m_ammoType = a_other.m_ammoType;
        m_reloadTime = a_other.m_reloadTime;
        m_accuracyPerShot = a_other.m_accuracyPerShot;
        m_tracerPrefab = a_other.m_tracerPrefab;
        m_accuracyRegain = a_other.m_accuracyRegain;
        m_internalMag = a_other.m_internalMag;
        m_cockingTime = a_other.m_cockingTime;
        m_aimDownSightSpeed = a_other.m_aimDownSightSpeed;
        m_ammoUsedPerShot = a_other.m_ammoUsedPerShot;
        m_defaultSight = a_other.m_defaultSight;
        m_magSprite = a_other.m_magSprite;
        m_aimAccuracy = a_other.m_aimAccuracy;
        m_loadedMagazine = a_other.m_loadedMagazine.GetCopy();

    }

    public void SetHolder(Entity a_holder)
    {
        m_holder = a_holder;
    }

    public void Attach(Attachment a_attachment)
    {
        if(a_attachment is Sight)
        {
            m_weaponObject.AttachSight((Sight)a_attachment.GetCopy());
        }
    }
    #region Shooting
    /// <summary>
    /// The Firing has 2 delays, one for the bullet to actually fire from the gun, and 1 for the damage after to simulate the bullet travel
    /// even though the raycast already has its calucaltions done we simulate a delay to give a sense of bullet velocity and when in slomo the bllet actually firing from the gun
    /// </summary>
    public void Fire()
    {
        if (!m_holder.IsFireLocked() &&  m_cocked)
        {
            if (HasMagazine() && !m_loadedMagazine.m_ammo.isEmpty)
            {
                m_currentAccuracy += m_accuracyPerShot;
                m_holder.StartCoroutine(RaycastAfterDelay());
                shotsFiredInBurst++;
                m_holder.Animator.SetTrigger("Fire");
                m_weaponObject.BulletCasingFX();
                m_weaponObject.Animator.SetTrigger("Fire");
                m_loadedMagazine.m_ammo.AdjustValue(-m_ammoUsedPerShot);
                m_cocked = false;
                if (m_fireMode == FireMode.BoltAction)
                {
                    m_holder.CockWeapon(0.35f,true);
                }
                else 
                {
                    m_holder.CockWeapon(0, true);
                } 
            }
        }
    }
    private IEnumerator RaycastAfterDelay()
    {
        yield return new WaitForSecondsRealtime (0.05f);
        RaycastShoot();
    }
    private void RaycastShoot()
    {
        RaycastHit hit;
        Vector3 dir = m_holder.m_camera.transform.forward;
        
        for (int i = 0; i < m_bulletsFiredPerShot; i++)
        {
            dir.y += Random.Range(-m_currentAccuracy / 10, m_currentAccuracy / 10);
            dir.z += Random.Range(-m_currentAccuracy / 10, m_currentAccuracy / 10);
           
            if (Physics.Raycast(m_holder.m_camera.transform.position, dir, out hit))
            {
                IDamagable hitObject = hit.collider.GetComponentInParent<IDamagable>();
                Rigidbody hitBone = hit.collider.GetComponent<Rigidbody>();
                m_holder.StartCoroutine(DelayedDamage(hitObject, hit.point, hit.normal, hitBone));
              
                BulletTracers b = Instantiate(m_tracerPrefab, m_weaponObject.m_muzzle.transform.position, Quaternion.identity);

                b.SetValues(m_weaponObject.m_muzzle.transform.position, hit.point, 0.1f);
            }
        }
        m_holder.GetComponentInChildren<SmoothLook>().recoil += new Vector3(m_recoilVerticalPerShot, 0, 0);
    }
    public IEnumerator DelayedDamage(IDamagable hitObject, Vector3 hitPoint, Vector3 hitNormal, Rigidbody hitBone)
    {
        yield return new WaitForSeconds(0.1f);
        BodyPart p = null;
        if (hitBone != null)
        {
            p = hitBone.GetComponent<BodyPart>();
        }
        float damageMod = 1;
        if(p != null)
        {
            switch (p.m_bodyPartType)
            {
                case BodyPart.BodyPartType.Head:
                    damageMod = 2;
                    break;
                case BodyPart.BodyPartType.Leg:
                    damageMod = 0.5f;
                    break;
            }

        }
        if (hitObject != null)
        {
            if (m_holder.m_playerControlled && !hitBone.GetComponentInParent<Entity>().isDead)
            {
                m_holder.m_hitMarker.SetTrigger("Hit");
            }
            hitObject.TakeDamage(m_kineticDamage * damageMod, m_energyDamage * damageMod, hitPoint, hitNormal, hitBone, m_holder);
            
        }
        else
        {
            GameObject impact = Instantiate(m_defaultBulletImpactPrefab, hitPoint, Quaternion.identity);
            impact.transform.forward = hitNormal;
            Destroy(impact, 2);
        }
    }
    #endregion
    public void LoadBullet()
    {
        m_loadedMagazine.m_ammo.m_current++;
    }
    public void LoadMagazine(Magazine a_mag)
    {
        m_loadedMagazine = a_mag;
    }
    public void UnloadMagazine()
    {
        m_loadedMagazine = null;
    }
    private bool HasMagazine()
    {
        if (m_loadedMagazine != null)
        {
            return true;
        }
        return false;
    }
}

[System.Serializable]
public class Magazine
{
    public Resource m_ammo;
    public enum AmmoType
    {
        AR,
        Pistol,
        ShotGun,
        SMG
    }
    public AmmoType m_ammoType;

    public virtual Magazine GetCopy()
    {
        Magazine i = new Magazine();
        i.Initialise(this);
        return i;
    }
    //Initialises all the needed variables for the copied Objct
    public virtual void Initialise(Magazine a_other)
    {
        m_ammo = new Resource();
        m_ammo.m_max = a_other.m_ammo.m_max;
        m_ammo.m_current = a_other.m_ammo.m_current;
        m_ammoType = a_other.m_ammoType;
        switch(m_ammoType)
        {
            case AmmoType.AR:
                m_ammo.m_max = 30;
                m_ammo.m_current = m_ammo.m_max;
                break;
            case AmmoType.Pistol:
                m_ammo.m_max = 9;
                m_ammo.m_current = m_ammo.m_max;
                break;
            case AmmoType.SMG:
                m_ammo.m_max = 30;
                m_ammo.m_current = m_ammo.m_max;
                break;
            case AmmoType.ShotGun:
                m_ammo.m_max = 6;
                m_ammo.m_current = m_ammo.m_max;
                break;
        }
    }
  
}

