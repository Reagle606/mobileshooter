﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponObject : MonoBehaviour, IInteractable
{
   
    public Weapon m_weapon;
    public Transform m_bulletEjectPoint;

    public Rigidbody[] m_bulletCasings;
    private int m_bulletCasingIndex = 0;
    private Animator m_animator;
    public Animator Animator { get { return m_animator; } }

    public Transform m_muzzle;
    Animator animator;

    public Transform m_sightHolder;

    public Collider m_collider;
    public Rigidbody m_rigidBody;

    public void Start()
    {
        Initialise();
        m_weapon = m_weapon.GetCopy();
        m_weapon.m_weaponObject = this;
        m_bulletCasingIndex = 0;
        if (m_weapon.m_currentSight == null)
        {
            m_weapon.Attach(m_weapon.m_defaultSight);
        }
    }
    public void Initialise()
    {
        m_animator = GetComponent<Animator>();
        m_collider = GetComponent<Collider>();
        m_rigidBody = GetComponent<Rigidbody>();
      

    }
    public void AttachSight(Sight a_sight)
    {
        SightObject s = (SightObject)Instantiate(a_sight.m_attachmentObjectPrefab ,m_sightHolder.transform.position,m_sightHolder.transform.rotation, m_sightHolder);
        a_sight.m_attachmentObject = s;
        m_weapon.m_currentSight = a_sight;
    }
    public void BulletCasingFX()
    {
        if (m_bulletCasings[m_bulletCasingIndex] == null)
        {
            m_bulletCasings[m_bulletCasingIndex] = Instantiate(m_weapon.m_bulletCasingPrefab, m_bulletEjectPoint.transform.position, m_bulletEjectPoint.transform.rotation).GetComponent<Rigidbody>();
        }
        m_bulletCasings[m_bulletCasingIndex].velocity = Vector3.zero;
        m_bulletCasings[m_bulletCasingIndex].transform.position = m_bulletEjectPoint.transform.position;
        m_bulletCasings[m_bulletCasingIndex].transform.rotation = m_bulletEjectPoint.transform.rotation;
        m_bulletCasings[m_bulletCasingIndex].gameObject.SetActive(true);
        m_bulletCasings[m_bulletCasingIndex].AddForce(m_bulletEjectPoint.transform.up * m_weapon.m_bulletCasingEjectForce, ForceMode.Impulse);
        m_bulletCasingIndex++;
        if (m_bulletCasingIndex > m_bulletCasings.Length - 1)
        {
            m_bulletCasingIndex = 0;
        }
    }

    public void Interact(Entity a_interactor)
    {
        a_interactor.DropWeapon();
        a_interactor.PickupWeapon(m_weapon);
        
    }
}
