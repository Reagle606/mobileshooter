﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSway : MonoBehaviour
{
    public float m_aimingSway;
    public float m_defaultSway;

    public float m_amount;
    public float m_smoothAmount;
    public float m_maxAmount;
    public Vector3 offsetMove;

    private Vector3 initialPosition;
  

    public void Start()
    {
        initialPosition = transform.localPosition;
     
    }

    public void Update()
    {
        float movementX = -Input.GetAxis("Mouse X") * m_amount;
        float movementY = -Input.GetAxis("Mouse Y") * m_amount;
        movementX = Mathf.Clamp(movementX, -m_maxAmount, m_maxAmount);
        movementY = Mathf.Clamp(movementY, -m_maxAmount, m_maxAmount);
        Vector3 finalPosition = new Vector3(movementX, movementY, 0);
        transform.localPosition = Vector3.Lerp(transform.localPosition, finalPosition + initialPosition, Time.deltaTime * m_smoothAmount);
      
    }
}
